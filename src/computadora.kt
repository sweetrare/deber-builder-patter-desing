


class Computer private constructor(builder: Builder) {

    class Builder(
        val RAM: String, val HDD: String,  val CPU: String,
        var bluethooth: String = "No",  var grafica: String = "No"
    ) {
        fun setGraphicsCardEnabled(isGraphicsCardEnabled: String): Builder {
            this.grafica = isGraphicsCardEnabled
            return this

        }

        fun setBluetoothEnabled(isBluetoothEnabled: String): Builder {
            this.bluethooth = isBluetoothEnabled
            return this
        }


        fun build(): Computer {

            return Computer(this)

        }

    }

    var ram: String
    var hdd: String
    val cpu: String
    var bluethooth: String
    var grafica: String

    init {
        this.hdd = builder.HDD
        this.cpu = builder.CPU
        this.ram = builder.RAM
        this.bluethooth = builder.isBluetoothEnabled
        this.grafica = builder.isGraphicsCardEnabled
    }

}
