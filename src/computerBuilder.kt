class ComputerBuilderDirector {


    val basicComputer: Computer
        get() = Computer.Builder("2GB", "2TB", "Intel i7").build()


    val graphicsCardEnabledComputer: Computer
        get() = Computer.Builder("2GB", "2TB", "Intel i7").setGraphicsCardEnabled("SI").build()

}